export type Theme = 'dark' | 'light'
export type Language = 'en' | 'it';

export interface ConfigState {
  theme: Theme;
  language: Language;
}

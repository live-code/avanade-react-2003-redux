import { combineReducers } from '@reduxjs/toolkit';
import { CartPanelStore } from './cart/cart-panel.store';
import { offersStore } from './clients/clients.store';
import { productsStore } from './products/products.store';

export const catalogReducers = combineReducers({
  products: productsStore.reducer,
  offers: offersStore.reducer,
  ui: CartPanelStore.reducer,

})

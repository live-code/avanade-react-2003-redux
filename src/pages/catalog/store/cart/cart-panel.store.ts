// core/store/http-status.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';


export const CartPanelStore = createSlice({
  name: 'httpStatus',
  initialState: {
    isOpenCartPanel: false,
    isOpenModal: false,
  },
  reducers: {
    openCartPanel(state) {
      state.isOpenCartPanel = true;
      state.isOpenModal = false;
    },
    closeCartPanel(state) {
      state.isOpenCartPanel = false;
    },

  }
})

export const {
  openCartPanel, closeCartPanel
} = CartPanelStore.actions;

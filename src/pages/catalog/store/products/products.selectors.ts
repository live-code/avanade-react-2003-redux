import { RootState } from '../../../../App';

export const selectProductsList = (state: RootState) => state.catalog.products.list;
/*
export const selectTotal = (state: RootState) => {
  let total = 0;
  state.catalog.forEach(item => {
    total += item.price
  })
  return total;
}
*/


export const selectTotal = (state: RootState) =>
  state.catalog.products.list.reduce((acc, item) => acc + item.price, 0)

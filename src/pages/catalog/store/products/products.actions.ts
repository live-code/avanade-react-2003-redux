import axios, { Axios } from 'axios';
import { AppThunk } from '../../../../App';
import { setHttpStatus } from '../../../../core/store/http-status.store';
import { Product } from '../../model/product';
import {
  addProductSuccess,
  deleteProductSuccess,
  getProductsSuccess,
  setError, setPending,
  toggleProductVisibility
} from './products.store';


export const getProducts = (): AppThunk => async (dispatch) => {
    dispatch(setPending())
    try {
      const response = await axios.get<Product[]>('http://localhost:3001/products')
      dispatch(setHttpStatus({ status: 'success', msg: 'products loaded'}))
      dispatch(getProductsSuccess(response.data))
    } catch (err) {
      // dispatch error
      dispatch(setHttpStatus({ status: 'error', msg: 'products not loaded'}))
      dispatch(setError())
    }
}

export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    dispatch(setError())
  }
};


export const addProduct = (product: Pick<Product, 'title' | 'price'>): AppThunk => async dispatch => {
  try {
    const newProduct = await axios.post<Product>('http://localhost:3001/products', {
        ...product,
        visibility: false
      }
    );
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    dispatch(setError())
  }
};
export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibility(response.data))
  } catch (err) {
    dispatch(setError())
  }
};

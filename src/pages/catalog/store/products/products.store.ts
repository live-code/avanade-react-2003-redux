import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Product } from '../../model/product';

export type ProductsState = {
  list: Product[],
  error: boolean,
  pending: boolean
}

const initialState: ProductsState = {
  list: [],
  error: false,
  pending: false
}

export const productsStore = createSlice({
  name: 'products',
  initialState: initialState,
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.list = action.payload;
      /*return {
        ...state,
        list: action.payload
      }*/
      state.pending = false;
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.list.push(action.payload);
      state.error = false;
      state.pending = false;
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      const index = state.list.findIndex(p => p.id === action.payload)
      state.list.splice(index, 1)
      state.error = false;
      state.pending = false;
    },
    toggleProductVisibility(state, action: PayloadAction<Product>) {
      const product = state.list.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
      state.error = false;
      state.pending = false;
    },
    setError(state) {
      state.error = true;
      state.pending = false;
    },
    setPending(state) {
      state.pending = true;
    }
  }
});

export const {
  addProductSuccess, getProductsSuccess, toggleProductVisibility, deleteProductSuccess,
  setError, setPending
} = productsStore.actions;

import React from 'react';
import { Product } from '../model/product';

interface ProductsListProps {
  items: Product[];
  onDelete: (id: number) => void;
  onToggle: (product: Product) => void;
}
export const ProductsList = (props: ProductsListProps) => {

  return (
    <div>
      {
        props.items.map((product: Product) => {
          {/*NEW: style visibility*/}
          return (
            <li
              className="list-group-item"
              style={{ opacity: product.visibility ? 1 : 0.5}}
              key={product.id}
            >
              {/*NEW*/}
              <span onClick={() => props.onToggle(product)}>
              {
                product.visibility ?
                  <i className="fa fa-eye" /> :
                  <i className="fa fa-eye-slash" />
              }
            </span>
              <span className="ml-2">{ product.title }</span>

              {/*NEW*/}
              <div className="pull-right">
                { product.price }
                <i
                  className="fa fa-trash "
                  onClick={(e) => props.onDelete(product.id)}
                />
              </div>

            </li>
          )
        })
      }
    </div>
  )
}

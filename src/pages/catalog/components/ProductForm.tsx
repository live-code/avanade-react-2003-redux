import React, { useState } from 'react';
import classnames from 'classnames';
import { Product } from '../model/product';

type FormType = Pick<Product, 'title' | 'price'>;
interface ProductsFormProps {
  // UPDATED
  onSubmit: (product: FormType) => void;
}

export const ProductForm: React.FC<ProductsFormProps> = props => {
  // UPDATED
  const [data, setData] = useState<FormType>({ title: '', price: 0});
  const titleIsValid = data.title && data.title.length > 3;
  // NEW
  const priceIsValid = data.price && data.price > 0;
  const valid = titleIsValid && priceIsValid;

  const onChangeHandler = (e: React.FormEvent<HTMLInputElement>) => {
    // UPDATED
    setData({
      ...data,
      [e.currentTarget.name]: e.currentTarget.value
    })
  };

  const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    // UPDATED
    props.onSubmit(data);
    setData({ title: '', price: 0});
  };

  return (
    <div className="example">
      <form onSubmit={onSubmitHandler}>
        {/*UPDATED*/}
        <input
          className={classnames(
            'form-control',
            { 'is-valid': titleIsValid },
            { 'is-invalid': !titleIsValid },
          )}
          type="text"
          placeholder="Write something..."
          onChange={onChangeHandler}
          name="title"
          value={data.title}
        />

        {/*NEW*/}
        <input
          className={classnames(
            'form-control',
            { 'is-valid': priceIsValid },
            { 'is-invalid': !priceIsValid },
          )}
          type="number"
          placeholder="Write something..."
          onChange={onChangeHandler}
          name="price"
          value={data.price}
        />
        {/*NEW*/}
        <button
          className="btn btn-primary"
          type="submit"
          disabled={!valid}>ADD</button>
      </form>
    </div>
  );
};

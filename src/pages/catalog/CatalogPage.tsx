import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../App';
import { openCartPanel } from './store/cart/cart-panel.store';
import { ProductForm } from './components/ProductForm';
import { ProductsList } from './components/ProductsList';
import { Product } from './model/product';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products/products.actions';
import { selectProductsList, selectTotal } from './store/products/products.selectors';

export const CatalogPage = () => {
  const dispatch = useAppDispatch();
  const error = useSelector((state: RootState) => state.catalog.products.error);
  const httpStatus = useSelector((state: RootState) => state.httpStatus);
  const pending = useSelector((state: RootState) => state.catalog.products.pending);
  const total = useSelector(selectTotal);
  const products = useSelector(selectProductsList)

  useEffect(() => {
    dispatch(getProducts())
  }, [dispatch])

  return <div>
    <h1>Catalog</h1>
    {httpStatus.status === 'success' && <div>{httpStatus.msg}</div>}
    {httpStatus.status === 'error' && <div>{httpStatus.msg}</div>}
    {pending && <div>loaders....</div>}

    <ProductForm onSubmit={(p) => dispatch(addProduct(p))} />

    <ProductsList
      items={products}
      onDelete={ (id: number) => dispatch(deleteProduct(id))}
      onToggle={p =>  dispatch(toggleProduct(p))}
    />

    <button onClick={() => dispatch(openCartPanel())}>open</button>

    {/*NEW*/}
    <div className="text-center">
      <div className="badge bg-info">Total: € {total}</div>
    </div>

  </div>
};

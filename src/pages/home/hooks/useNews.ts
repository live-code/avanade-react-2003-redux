import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectNewsList } from '../store/news.selectors';
import { addNews, deleteNews, FilterType, setFilter, toggleNews } from '../store/news.store';

export const useNews = () => {
  const dispatch = useDispatch();
  const newsList = useSelector(selectNewsList);

  function onKeyHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = ''
    }
  }
  function setVisibilityFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    dispatch(setFilter(e.currentTarget.value as FilterType))
  }

  function deleteNewsHandler(id: number) {
    dispatch(deleteNews(id))
  }

  function toggleNewsHandler(id: number) {
    dispatch(toggleNews(id))
  }

  return {
    newsList,
    onKeyHandler,
    dispatch,
    deleteNewsHandler,
    toggleNewsHandler,
    setVisibilityFilter
  }
}

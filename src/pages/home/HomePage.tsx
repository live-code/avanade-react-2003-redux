import classNames from 'classnames';
import React from 'react';
import { useNews } from './hooks/useNews';

export const HomePage = () => {
  const {
    newsList,
    deleteNewsHandler,
    toggleNewsHandler,
    onKeyHandler,
    setVisibilityFilter
  } = useNews();

  return <div>
    HomePage

    <input
      type="text" className="form-control" placeholder="news title"
      onKeyDown={onKeyHandler}
    />
    <hr/>

    <select className="form-control" onChange={setVisibilityFilter}>
      <option value="all">Show all</option>
      <option value="published">Published only</option>
      <option value="unpublished">Unpublished only</option>
    </select>

    {
      newsList.map(news => {
        return <li key={news.id}>
          {news.title}
          <i
            className={classNames(
              'fa',
              {
                'fa-eye-slash': !news.published,
                'fa-eye': news.published
              }
            )}
            onClick={() => toggleNewsHandler(news.id)}
          ></i>
          <button onClick={() => deleteNewsHandler(news.id)}>Delete</button>
        </li>
      })
    }

  </div>
};

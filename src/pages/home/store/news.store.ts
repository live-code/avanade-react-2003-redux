import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { News } from '../model/news';

export type FilterType = 'all' | 'published' | 'unpublished';

interface NewsStoreState {
  filter: FilterType,
  list: News[]
}

const initialState: NewsStoreState = {
  filter: 'all',
  list: []
}

export const newsStore = createSlice({
  name: 'home',
  initialState,
  reducers: {
    setFilter(state, action: PayloadAction<FilterType>) {
      state.filter = action.payload;
    },
    addNews(state, action: PayloadAction<string>) {
      const newItem = {
        id: Date.now(),
        title: action.payload,
        published: false
      };
      state.list.push(newItem)
    },
    deleteNews(state, action: PayloadAction<number>) {
      const index = state.list.findIndex(news => news.id === action.payload)
      state.list.splice(index, 1)
    },
    toggleNews(state, action: PayloadAction<number>) {
      const newsToUpdate = state.list.find(news => news.id === action.payload)
      if (newsToUpdate) {
        newsToUpdate.published = !newsToUpdate.published;
      }
    },
  }
})

export const {
  addNews,
  setFilter,
  deleteNews,
  toggleNews,
} = newsStore.actions

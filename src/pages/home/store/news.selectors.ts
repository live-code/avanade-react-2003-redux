import { RootState } from '../../../App';

export const selectNewsList = (state: RootState) => {
  switch (state.news.filter) {
    case 'all': return state.news.list;
    case 'published': return state.news.list.filter(n => n.published)
    case 'unpublished': return state.news.list.filter(n => !n.published)
  }
}

import { useDispatch } from 'react-redux';
import { changeConfig, changeLanguage, changeTheme } from '../../core/store/config.store';

export const SettingsPage = () => {
  const dispatch = useDispatch()


  return <div>
    SettingsPage

    <button onClick={() => dispatch(changeTheme('light'))}>set light</button>
    <button onClick={() => dispatch(changeTheme('dark'))}>set dark</button>
    <button onClick={() => dispatch(changeLanguage('it'))}>it</button>
    <button onClick={() => dispatch(changeLanguage('en'))}>en</button>
    <button onClick={() => dispatch(changeConfig({ theme: 'light', language: 'en'}))}>update config 1</button>
    <button onClick={() => dispatch(changeConfig({ theme: 'dark', language: 'en'}))}>update config 2</button>
  </div>
};

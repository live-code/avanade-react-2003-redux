export interface CounterConfig {
  itemsPerPallet: number;
  material: 'wood' | 'plastic'
}

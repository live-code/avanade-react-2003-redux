import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { setCounterConfig } from './store/config/counter-config.actions';
import { decrement, increment, reset } from './store/counter/counter.actions';
import { selectCounter, selectTotalPallets } from './store/counter/counter.selectors';


export const CounterPage = () => {

  const dispatch = useDispatch();
  const counter = useSelector(selectCounter);
  const totalPallets = useSelector(selectTotalPallets)
  const material = useSelector((state: RootState) => state.counter.config.material)


  return <div>
    <div>Tot Products: {counter}</div>
    <div>Tot Pallets: {totalPallets}</div>
    <div>material: {material}</div>

    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setCounterConfig({ material: 'wood'}))}>wood</button>
    <button onClick={() => dispatch(setCounterConfig({ material: 'plastic'}))}>plastic</button>
    {/*<button onClick={() => dispatch(setCounterConfig({ itemsPerPallet: 5}))}>Pallet 5</button>
    <button onClick={() => dispatch(setCounterConfig({ itemsPerPallet: 10}))}>Pallet 10</button>*/}
  </div>
};

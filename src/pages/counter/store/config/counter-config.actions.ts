import { createAction } from '@reduxjs/toolkit';
import { CounterConfig } from '../../model/counter-config';

export const setCounterConfig = createAction<Partial<CounterConfig>>('setCounterConfig')

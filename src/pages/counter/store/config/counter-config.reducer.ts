import { createReducer } from '@reduxjs/toolkit';
import { CounterConfig } from '../../model/counter-config';
import { setCounterConfig } from './counter-config.actions';

const initialState: CounterConfig = {
  itemsPerPallet: 5,
  material: 'wood'
}
export const counterConfigReducer = createReducer(initialState, builer =>
  builer
    .addCase(setCounterConfig, (state, action) => {
      if (action.payload.material === 'wood') {
        return { ...state, ...action.payload, itemsPerPallet: 5 }
      }
      return { ...state, ...action.payload, itemsPerPallet: 10 }
    })
)

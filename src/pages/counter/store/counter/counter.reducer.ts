import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { decrement, increment, reset } from './counter.actions';


export const counterReducer = createReducer(0, builder =>
  builder
    .addCase(increment, (state, action ) => state + action.payload)
    .addCase(decrement, (state, action ) => state - action.payload)
    .addCase(reset, () => 0)
)

/*
export const counterReducer = createReducer(0, {
  [increment.type]: (state, action: PayloadAction<number>) =>  state + action.payload,
  [decrement.type]: (state, action: PayloadAction<number>) =>  state - action.payload,
  [reset.type]: () =>  0
})*/
/*

export function counterReducer(state = 0, action: any) {
  console.log('qui!', action)
  switch(action.type) {
    case increment.type:
      return state + action.payload;

    case decrement.type:
      return state - action.payload;

    case reset.type:
      return 0
  }
  return state
}
*/

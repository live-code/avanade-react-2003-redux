import { Action, AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import React from 'react';
import { Root } from 'react-dom/client';
import { Provider, useDispatch } from 'react-redux';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import { Navbar } from './core/components/NavBar';
import { CartPanelStore } from './pages/catalog/store/cart/cart-panel.store';
import { configStore } from './core/store/config.store';
import { httpStatusStore } from './core/store/http-status.store';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { catalogReducers } from './pages/catalog/store';
import { offersStore } from './pages/catalog/store/clients/clients.store';
import { productsStore } from './pages/catalog/store/products/products.store';
import { CounterPage } from './pages/counter/CounterPage';
import { counterReducers } from './pages/counter/store';
import { HomePage } from './pages/home/HomePage';
import { newsStore } from './pages/home/store/news.store';
import { SettingsPage } from './pages/settings/SettingsPage';
import { UsersPage } from './pages/users/UsersPage';

const rootReducer = combineReducers({
  httpStatus: httpStatusStore.reducer,
  news: newsStore.reducer,
  catalog: catalogReducers,
  counter: counterReducers,
  config: configStore.reducer
})

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>;
export type AppDispatch = ThunkDispatch<RootState, any, AnyAction>;
export const useAppDispatch = () => useDispatch<AppDispatch>()


function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/home" element={<HomePage />} />
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/catalog" element={<CatalogPage />} />
          <Route path="/settings" element={<SettingsPage />} />
          <Route
            path="*"
            element={
              <Navigate to='/home' />
            }
          />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ConfigState, Language, Theme } from '../../model/config';

const initialState: ConfigState = {
  theme: 'dark',
  language: 'it',
}

export const configStore = createSlice({
  name: 'config',
  initialState,
  reducers: {
    changeTheme(state, action: PayloadAction<Theme>) {
      // return { ...state, theme: action.payload}
      state.theme = action.payload;
    },
    changeLanguage(state, action: PayloadAction<Language>) {
      state.language = action.payload
    },
    changeConfig(state, action: PayloadAction<Partial<ConfigState>>) {
       return {
         ...state,
         ...action.payload
       }
    }
  }
})

export const { changeTheme, changeLanguage, changeConfig  } = configStore.actions;


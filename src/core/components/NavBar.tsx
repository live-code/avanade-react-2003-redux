import classNames from 'classnames';
import React from 'react';
import { Root } from 'react-dom/client';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RootState, useAppDispatch } from '../../App';
import { selectCounter } from '../../pages/counter/store/counter/counter.selectors';
import { closeCartPanel, openCartPanel } from '../../pages/catalog/store/cart/cart-panel.store';


type ActiveType = { isActive: boolean };

const activeStyleHandler = (obj: ActiveType) => {
  return obj.isActive ? { color: 'red'} : {}
}


export const Navbar = () => {
  const theme = useSelector((state: RootState) => state.config.theme)
  const counter = useSelector(selectCounter)
  const isOpenCartPanel = useSelector((state: RootState) => state.catalog.ui.isOpenCartPanel)
  const dispatch = useAppDispatch();

  console.log('theme', theme)
  return (
    <nav className={classNames(
      'navbar navbar-expand',
      {
        'navbar-dark bg-dark': theme === 'dark',
        'navbar-light bg-light': theme === 'light'
      }
    )}>

      {
        isOpenCartPanel &&
          <div style={{ background: 'gray', width: 100, height: 100, position: 'fixed', top: 10, right: 10}}>
            panel

            <button onClick={() => dispatch(closeCartPanel())}>close</button>
          </div>
      }

      <div className="navbar-brand">
        <NavLink
          style={activeStyleHandler}
          className="nav-link"
          to="/">
          REDUX {counter}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink style={activeStyleHandler}
                     className="nav-link"
                     to="/settings">
              <small>settings</small>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              style={ activeStyleHandler}
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={activeStyleHandler}
              className="nav-link"
              to="/users">
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={activeStyleHandler}
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}
